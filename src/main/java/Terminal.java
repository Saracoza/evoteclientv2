import communication.*;

import java.io.*;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.zip.DataFormatException;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

class Terminal
{
    protected String response;

    // receive data
    // from  server
    private DataInputStream recvData;

    // send data
    // to server
    private DataOutputStream sendData;



    Terminal(Socket socket) throws IOException
    {
        responseBuffer = new byte[8192];

        recvData = new DataInputStream (socket.getInputStream() );
        sendData = new DataOutputStream(socket.getOutputStream());
    }



    @SuppressWarnings("FieldCanBeLocal")
    private int    responseLength;
    private byte[] responseBuffer;

    protected void putCommand(String cmd, String arg) throws IOException
    {
        String command = Serializer.serializeCommand(cmd, arg);

        sendData.writeInt(command.length());

        sendData.write(command.getBytes());
    }

    protected String getResponse() throws IOException, DataFormatException
    {
        responseLength = recvData.readInt();

        recvData.readFully(responseBuffer, 0, responseLength);

        var typeAndText = Deserializer.deserializeResponse(new String(responseBuffer, 0, responseLength));
        var type = typeAndText.getValue0();
        var text = typeAndText.getValue1();

        switch (type) {
            default:      { return null;                                        }
            case OK:      { return text;                                        }
            case Success: { return ansi().fg(GREEN).a(text).reset().toString(); }
            case Error:   { throw new RemoteException(text);                    }
        }
    }
}
