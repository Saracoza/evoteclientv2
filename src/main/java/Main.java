import conf.SettingsChain;

import org.fusesource.jansi.AnsiConsole;
import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        AnsiConsole.systemInstall();

        System.out.println(ansi().fg(WHITE).a("Connecting...").reset());
        try (var socket = new java.net.Socket(
                                 SettingsChain.createInstance().getSettingValue("serverAddress"),
                Integer.parseInt(SettingsChain.createInstance().getSettingValue("serverPort")))) {
            System.out.println(ansi().fg(GREEN).a("Connected").reset());


            uprove.storage.TokensManager.createInstance();


            TerminalSimple terminalSimple = new TerminalSimple(socket);
            String cmd = "help";
            Scanner in = new Scanner(System.in);


            util.Helper.createInstance("help.txt");
            while (!cmd.equals("exit")) {
                if (cmd.equals("help")) {
                    util.Helper.getInstance().displayHelp();
                } else {
                    terminalSimple.putCommand(cmd);
                    terminalSimple.printResponse();
                }
                terminalSimple.printPathCTX();

                cmd = in.nextLine();
            }
        } catch (java.io.IOException e) {
            System.out.print(ansi().fg(RED));
            System.out.println("ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }

        AnsiConsole.systemUninstall();
    }
}
