import com.microsoft.uprove.*;

import authentication.AuthHelper;
import uprove.controllers.UserController;
import uprove.converters.*;
import uprove.exceptions.*;
import uprove.storage.TokensManager;

import java.io.IOException;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.zip.DataFormatException;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

class TerminalVote extends Terminal
{
    private static final String noOptionError      = ansi().fg(RED).a("Option does not exist!"      ).toString();
    private static final String notIntTokenIdError = ansi().fg(RED).a("Token ID must be an integer!").toString();
    private final String colorOptionID = ansi().fg(CYAN).a("Option ID: ").reset().toString();
    private final String colorTokenID  = ansi().fg(CYAN).a("Token ID: " ).reset().toString();
    private final String noARG = "";

    private java.util.Scanner in = new java.util.Scanner(System.in);

    private UserController userController;



    TerminalVote(Socket socket) throws IOException
    {
        super(socket);

        putCommand("init", noARG);
        try {
            userController = new UserController(Deserializer.deserializeIssuerParameters(getResponse()));
        } catch (UProveException |
                 RemoteException |
                 DataFormatException e) {
            throw new IOException("An error occurred during issuer parameters setup.");
        }
    }



    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean checkOption(String[] optionIDs, String optionID)
    {
        for (var crtOptionID : optionIDs) {
            if (crtOptionID.equals(optionID)) {
                return true;
            }
        }
        return false;
    }

    private void endProtocol(String cmd, String optionID, UProveToken token, PresentationProof proof)
    {
        var serverAddress = conf.SettingsChain.getInstance().getSettingValue("serverAddress");
        var serverPort    = conf.SettingsChain.getInstance().getSettingValue("serverPort"   );
        try (var socket = new java.net.Socket(serverAddress, Integer.parseInt(serverPort))) {


            Terminal terminal = new Terminal(socket);
            terminal.putCommand(
                    cmd,
                    Serializer.serializeMessageTokenProof(
                            optionID.getBytes(),
                            token,
                            proof
                    )
            );


            response = terminal.getResponse();

        } catch (DataFormatException | RemoteException e) {

            response = ansi().fg(RED).a(e.getMessage()).reset().toString();

        } catch (IOException e) {
            System.out.print(ansi().fg(RED));
            System.out.println("ERROR > " + e.getMessage());
            System.out.print(ansi().reset());
        }
    }



    String vote(String optionID) throws IOException
    {
        byte[][] authenticationObject = null;
        UProveKeyAndToken keyAndToken = null;

        response = "";
        try {
            putCommand("getselpoll", noARG); var pollName = getResponse();
            putCommand("getoptions", noARG); var optnsIDs = getResponse().split("::");

            if (!checkOption(optnsIDs, optionID)) return noOptionError;



            putCommand("auth",
                    AuthHelper.serializeAuthObj(authenticationObject = AuthHelper.buildAuthObj())
            );
            System.out.println(getResponse());

            userController.initialize(pollName.getBytes(), authenticationObject);
            putCommand("genmsg1", noARG);
            putCommand("genmsg3",
                    Serializer.bytesToBase64(userController.generateSecondMessage(Deserializer.base64ToBytes(getResponse())))
            );
            keyAndToken = userController.generateToken(Deserializer.base64ToBytes(getResponse()));
            var proof   = userController.generateProof(
                    optionID.getBytes(),
                    keyAndToken,
                    authenticationObject
            );



            TokensManager.getInstance().storeToken(keyAndToken);



            endProtocol("vote", optionID, keyAndToken.getToken(), proof);
        } catch (UProveException |
                 RemoteException |
                 DataFormatException e) {
            return ansi().fg(RED).a(e.getMessage()).reset().toString();
        } finally {
            AuthHelper.clearAuthObj(authenticationObject);
            userController.clearPrivKeyToken(keyAndToken);
        }
        return response;
    }



    String updVote() throws IOException
    {
        byte[][] authenticationObject = null;
        UProveKeyAndToken keyAndToken = null;

        response = "";
        try {
            int tokenID;
            System.out.println("Tokens IDs:");
            System.out.println("-----------");
            TokensManager.getInstance().listTokens();
            System.out.println("-----------");
            System.out.print(colorTokenID);
            try                 { tokenID = Integer.parseInt(in.nextLine()); }
            catch (Exception e) { return notIntTokenIdError;                 }


            keyAndToken = TokensManager.getInstance().loadToken(tokenID);


            var pollName = new String(keyAndToken.getToken().getTokenInformation());
            putCommand("selpoll", pollName);
            getResponse();
            putCommand("getoptions", noARG);
            var optionsIDs = getResponse().split("::");
            System.out.println("Poll   name: " + pollName);
            System.out.println("Options IDs: "           );
            for (var optionID : optionsIDs) System.out.println(optionID);
            System.out.print(colorOptionID);
            String optionID = in.nextLine();
            if (!checkOption(optionsIDs, optionID)) return noOptionError;


            var proof = userController.generateProof(optionID.getBytes(), keyAndToken, authenticationObject = AuthHelper.buildAuthObj());


            endProtocol("updvote", optionID, keyAndToken.getToken(), proof);
        } catch (UProveException |
                 RemoteException |
                 DataFormatException e) {
            return ansi().fg(RED).a(e.getMessage()).reset().toString();
        } finally {
            AuthHelper.clearAuthObj(authenticationObject);
            userController.clearPrivKeyToken(keyAndToken);
        }
        return response;
    }
}
