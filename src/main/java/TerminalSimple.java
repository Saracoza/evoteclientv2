import java.io.IOException;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.zip.DataFormatException;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;

class TerminalSimple extends Terminal
{
    private TerminalVote terminalVote;



    TerminalSimple(Socket socket) throws IOException
    {
        super(socket);

        terminalVote = new TerminalVote(socket);
    }



    void putCommand(String command) throws IOException
    {
        var cmdAndArg = command.split("\\p{javaWhitespace}+", 2);
        var cmd = cmdAndArg[0];
        var arg = cmdAndArg.length == 2 ? cmdAndArg[1] : "";

        switch (cmd) {
            case    "vote": { response = terminalVote.vote(arg); return; }
            case "updvote": { response = terminalVote.updVote(); return; }
        }

        putCommand(cmd, arg);
        try                                             { response = getResponse();                                       }
        catch (DataFormatException | RemoteException e) { response = ansi().fg(RED).a(e.getMessage()).reset().toString(); }
    }

    void printResponse()
    {
        if (response.equals("")) System.out.print  (response);
        else                     System.out.println(response);
    }



    void printPathCTX() throws IOException
    {
        try {
            putCommand("getpathctx", "");

            var pathCTX = getResponse().split(" > ");

            System.out.print(ansi().bgBright(WHITE).fg(BLUE).a(pathCTX[0]).reset().a(" > "));
            if (pathCTX.length == 2) {
                System.out.print(ansi().bg(YELLOW).fg(BLACK).a(pathCTX[1]).reset().a(" > "));
            }
        } catch (DataFormatException | RemoteException e) {
            System.out.print(ansi().fg(RED));
            System.out.print("ERROR > Cannot obtain context path... > ");
            System.out.print(ansi().reset());
        }
    }
}
